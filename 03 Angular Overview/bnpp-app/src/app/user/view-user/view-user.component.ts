import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs';
import { takeWhile } from '../../../../node_modules/rxjs/operators';
import { User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css'],
})
export class ViewUserComponent implements OnInit {
  title = "View all users";
  tableTitle = "All the users";
  everythingBlue = true;
  showStructuralDirective = true;
  alive = true;
  users: User[];

  myClasses = {
    underline: true,
    pink: true,
    large: false
  }

  myStyles: {
    backgroundColor: string,
    textDecoration: string
  }

  //please don't use aliases as standard practice
  @Input('users')
  superUsers: User[];

  @Input()
  otherThings;

  @Output()
  favourite = new EventEmitter();

  constructor(private userService: UserService) { }

  ngOnInit() {
    //3 functions: onNext, onError, onComplete
    this.userService.getUsers().subscribe(users=>this.users=users);
  }



  highlight(evt: MouseEvent) {
    (<HTMLElement>evt.target).style.backgroundColor = "pink";
  }

  toggleStyles() {
    this.myStyles = {
      backgroundColor: this.everythingBlue ? 'deepskyblue' : 'hotpink',
      textDecoration: 'underline'
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
