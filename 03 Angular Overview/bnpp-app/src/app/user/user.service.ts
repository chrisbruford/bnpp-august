import { Injectable } from '@angular/core';
import { of, Observable } from '../../../node_modules/rxjs';
import { delay, map } from '../../../node_modules/rxjs/operators';
import { User } from './user.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // users: User[];
  users = of([
    { firstname: 'Stephen', surname: 'Cunningham', hobby: 'Golf' },
    { firstname: 'Philip', surname: 'McCarthy', hobby: 'Weightlifting' },
    { firstname: 'Colm', surname: 'O\'Grady', hobby: 'Football' },
    { firstname: 'Chris', surname: 'Bruford', hobby: 'Scuba' }
  ]).pipe(
    //operators
    delay(3000),
    map((users)=>{
      for (let user of users) {
        user.surname = 'Bruford'
      }
      return users
    })
  )

  constructor(private http: HttpClient) {
    // http.get<User[]>('http://www.bnppthings.com/users').subscribe(users=>this.users = users);
   }

  getUsers(): Observable<User[]> {
    // in reality some logic to fetch users from service goes here
    return this.users;
  }
}
