import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '../../../../node_modules/@angular/forms';
import { User } from '../user.model';
import { NoSmith } from '../../shared/validators/no-smith.validator';
import { BNPPValidators } from '../../shared/validators/BNPPValidators';
// import { noSmith } from '../../shared/validators/no-smith.validator';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  @Output()
  newUser = new EventEmitter<User>();

  addUserForm: FormGroup;
  
  // new FormGroup({
  //   firstname: new FormControl(),
  //   surname: new FormControl(),
  //   hobby: new FormControl()
  // });

  constructor(private fb: FormBuilder) { 
    this.addUserForm = fb.group({
      firstname: ['',[Validators.required, Validators.maxLength(8)]],
      surname: ['',[Validators.required, BNPPValidators.noSmith]],
      hobby: ['',[Validators.required]],
      email: ['',[Validators.required, Validators.email]]
    });
  }

  ngOnInit() {
  }

  doSubmit() {
    let newUser = new User();
    //populate from the form
    newUser.firstname = this.addUserForm.get('firstname').value;
    newUser.surname = this.addUserForm.get('surname').value;
    newUser.hobby = this.addUserForm.get('hobby').value;

    this.newUser.emit(newUser);
  }

  populateForm() {
    this.addUserForm.setValue({
      firstname: 'Michael',
      surname: 'Collins',
      hobby: 'Hurling',
      email: 'm@collins.com'
    });
  }

  makeBruford() {
    this.addUserForm.patchValue({
      surname: 'Bruford'
    });
  }

}
