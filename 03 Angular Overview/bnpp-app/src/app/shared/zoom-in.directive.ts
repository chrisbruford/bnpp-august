import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appZoomIn]'
})
export class ZoomInDirective implements OnInit {

  @Input()
  appZoomIn = "3em";

  @Input()
  color;

  constructor(private element: ElementRef) { }
  
  ngOnInit() {
    (<HTMLElement>this.element.nativeElement).style.fontSize = this.appZoomIn;
    (<HTMLElement>this.element.nativeElement).style.color = this.color;
  }

}
