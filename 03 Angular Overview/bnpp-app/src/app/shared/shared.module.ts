import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZoomInDirective } from './zoom-in.directive';
import { NoSmith } from './validators/no-smith.validator';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ZoomInDirective, NoSmith],
  exports: [ZoomInDirective, NoSmith]
})
export class SharedModule { }
