import { AbstractControl, ValidatorFn } from "../../../../node_modules/@angular/forms";

export abstract class BNPPValidators {

    static noSmith(control: AbstractControl) {
        const invalid = /Smith/i.test(control.value);

        return invalid ? { 'noSmith': { value: control.value } } : null;
    }
}