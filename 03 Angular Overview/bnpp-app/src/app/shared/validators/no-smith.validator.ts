import { Directive } from "../../../../node_modules/@angular/core";
import { Validators, ValidatorFn, Validator, AbstractControl, NG_VALIDATORS } from "../../../../node_modules/@angular/forms";

// import { ValidatorFn, AbstractControl } from "../../../../node_modules/@angular/forms";

// export let noSmith: ValidatorFn = (control: AbstractControl) => {
//     const invalid = /Smith/i.test(control.value);

//     return invalid ? { 'noSmith': { value: control.value } } : null;
// }

@Directive({
    selector: '[nosmith]',
    providers: [{provide: NG_VALIDATORS, useExisting: NoSmith, multi: true}]

}) export class NoSmith implements Validator {

    validate(control: AbstractControl) {
        const invalid = /Smith/i.test(control.value);

        return invalid ? { 'noSmith': { value: control.value } } : null;
    }

}