import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { of, Observable } from '../../node_modules/rxjs';
import { delay, map } from 'rxjs/operators';
import { User } from './user/user.model';
import { UserService } from './user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Wonderful';
  logo = '../assets/logo.png';
  displayLogo = true;
  favouriteUser;

  users: User[];

  constructor(
    private userService: UserService
  ) {
    userService.getUsers().subscribe(
      users => this.users = users,
      err => console.error(err)
    )
  }

  setFavourite(user) {
    this.favouriteUser = user;
  }

  doNewUser(user: User) {
    console.log(`New user added: ${user.firstname}`);
  }
}
