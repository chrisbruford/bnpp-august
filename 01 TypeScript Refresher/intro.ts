console.log('Hello world');
//types
let myObject: Object = {};
let myString: string = "Hello world";
let myNumber: number = 50;
let myBoolean: boolean = true;
let myNull: null = null;
let myUndefined;
// let mySymbol = Symbol();

function sum(a: number,b: number) {
    return a+b
}

console.log(sum(2,2));
// console.log(sum("a","b"));
// console.log(sum([],{}));

//Arrays
let myArray: string[] = ["1","true","things"];
//Tuple
let person: [string,number,boolean];
person = ["hello",12,true];

//Enums
enum Color {Red, Green, Blue, Orange}

function changeColour(target: {color: Color},color: Color): void {
    target.color = color;
}

let myCar = {
    make: 'Ford',
    model: 'Fiesta',
    color: Color.Blue
}

changeColour(myCar, Color.Green);


//Type Assertion
let fruit: any = 'bananas';
(<string>fruit).includes('anan');
(fruit as string).includes('anan');

//classes

class Car {
    private _wheels = 4;
    private _speed = 0;
    static quantity = 0;
    
    constructor(
        public make: string, 
        public model: string
    ) {
        Car.quantity++
    }

    accelerate(speed: number) {
        this._speed = speed;
    }

    brake(speed: number) {
        this._speed = speed;
    }

    get speed() {
        return this._speed;
    }

    set wheels(wheels: number) {
        this._wheels = wheels;
    }
}

console.log(Car.quantity)
let myCar1 = new Car('Ford','Fiesta');
let myCar2 = new Car('Skoda','Octavia');
let myCar3 = new Car('Nissan','Qashqai');
let myCar4 = new Car('BMW','325ci');

console.log(Car.quantity)