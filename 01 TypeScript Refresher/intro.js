console.log('Hello world');
//types
let myObject = {};
let myString = "Hello world";
let myNumber = 50;
let myBoolean = true;
let myNull = null;
let myUndefined;
// let mySymbol = Symbol();
function sum(a, b) {
    return a + b;
}
console.log(sum(2, 2));
// console.log(sum("a","b"));
// console.log(sum([],{}));
//Arrays
let myArray = ["1", "true", "things"];
//Tuple
let person;
person = ["hello", 12, true];
//Enums
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
    Color[Color["Orange"] = 3] = "Orange";
})(Color || (Color = {}));
function changeColour(target, color) {
    target.color = color;
}
let myCar = {
    make: 'Ford',
    model: 'Fiesta',
    color: Color.Blue
};
changeColour(myCar, Color.Green);
//Type Assertion
let fruit = 'bananas';
fruit.includes('anan');
fruit.includes('anan');
//classes
class Car {
    constructor(make, model) {
        this.make = make;
        this.model = model;
        this._wheels = 4;
        this._speed = 0;
        Car.quantity++;
    }
    accelerate(speed) {
        this._speed = speed;
    }
    brake(speed) {
        this._speed = speed;
    }
    get speed() {
        return this._speed;
    }
    set wheels(wheels) {
        this._wheels = wheels;
    }
}
Car.quantity = 0;
console.log(Car.quantity);
let myCar1 = new Car('Ford', 'Fiesta');
let myCar2 = new Car('Skoda', 'Octavia');
let myCar3 = new Car('Nissan', 'Qashqai');
let myCar4 = new Car('BMW', '325ci');
console.log(Car.quantity);
